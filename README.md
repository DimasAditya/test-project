# Story 6 

## Anggota
Dimas Aditya Faiz 1706039686


## Status Aplikasi
[![Pipeline](https://gitlab.com/DimasAditya/test-project/badges/master/pipeline.svg)](https://gitlab.com/DimasAditya/test-project/commits/master)
[![coverage report](https://gitlab.com/DimasAditya/test-project/badges/master/coverage.svg)](https://gitlab.com/DimasAditya/test-project/commits/master)

## Link Heroku App
https://dfaiz-test.herokuapp.com/
