from django.shortcuts import render
from .models import Status
from django.http import HttpResponseRedirect
from .forms import StatusForms
# Create your views here.

def index(request):
	tmp = Status.objects.all()
	if request.method == 'POST':
		form =  StatusForms(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect('/')
	else:
		form = StatusForms()
	return render(request, 'landing.html', {'form':form, 'data':tmp})

def profile(request):
	return render(request, 'profile.html')