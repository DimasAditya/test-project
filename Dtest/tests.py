from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .models import Status
from .forms import StatusForms
from .views import index, profile

from selenium import webdriver
import unittest
import time


# Create your tests here.
class HomeTest(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_function_caller_exist(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_if_template_landing_is_used(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'landing.html')
	
# chalenge
class ProfileTestPage(TestCase):
	def test_url_exist(self):
		response = Client().get('/profile')
		self.assertEqual(response.status_code, 200)

	def test_function_caller_exist(self):
		found = resolve('/profile')
		self.assertEqual(found.func, profile)

# Create your tests here.
class NewVisitorTest(unittest.TestCase):
	def setUp(self):
		self.browser = webdriver.Chrome()

	def test_can_start_a_list_and_retrieve_it_later(self):
		self.browser.get('https://dfaiz-test.herokuapp.com')
		time.sleep(5) # Let the user actually see something!
		search_box = self.browser.find_element_by_name('status')
		search_box.send_keys('Coba coba')
		search_box.submit()
		time.sleep(5) # Let the user actually see something!
		self.assertIn( "Coba coba", self.browser.page_source)

